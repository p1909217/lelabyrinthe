#include "Personnage.h"


Personnage::Personnage(){

    x = 15; y = 0;

}


void Personnage::persGauche (const Labyrinthe & l) {
	if (l.estPositionValide(x-1,y)) x--;
}

void Personnage::persDroite (const Labyrinthe & l) {
	if (l.estPositionValide(x+1,y)) x++;
}

void Personnage::persHaut (const Labyrinthe & l) {
	if (l.estPositionValide(x,y+1)) y++;
}

void Personnage::persBas (const Labyrinthe & l) {
	if (l.estPositionValide(x,y-1)) y--;
}

/*void attaquer(){
}*/

/*void ouvrirPorte(const Cle &c, const Porte &p){
}

void ramasseArme(const Labyrinthe &l, const Arme &a){
}
*/

int Personnage::persGetX () const { return x; }

int Personnage::persGetY () const { return y; }

void Personnage::persSetX (unsigned int px)  { x = px; }

void Personnage::persSetY (unsigned int py)  { y = py; }

void Personnage::Mort (const Ennemi &en)
{
        if(x == en.EnnGetX() && y == en.EnnGetY())
        {
        persSetX(15);
        persSetY(0);
        }

}
