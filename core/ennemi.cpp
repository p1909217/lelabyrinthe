#include "Ennemi.h"
#include <math.h>

Ennemi::Ennemi(){

    x = 1; y = 2;
    dir = 0;

}

void Ennemi::gauche (const Labyrinthe &l) {
	if (l.estPositionValide(x-1,y)) x--;
}

void Ennemi::droite (const Labyrinthe &l) {
	if (l.estPositionValide(x+1,y)) x++;
}

void Ennemi::haut (const Labyrinthe &l) {
	if (l.estPositionValide(x,y+1)) y++;
}

void Ennemi::bas (const Labyrinthe &l) {
	if (l.estPositionValide(x,y-1)) y--;
}

int Ennemi::EnnGetX () const { return x; }

int Ennemi::EnnGetY () const { return y; }

void Ennemi::bougeAuto (const Labyrinthe &l) {
    if(l.estPositionValide(x+1,y)){
        x = x+1;
    }
    else if(!l.estPositionValide(x+1,y)){
        x = x-10;
    }







    /*if(l.estPositionValide(x+1,y)){
            x = x+1;
    }
    else if( && l.estPositionValide(x-1,y)){
            x = x-2;
    }*/

    /*int dx [4] = { 1, 0, -1, 0};
    int dy [4] = { 0, 1, 0, -1};
    int xtmp,ytmp;
    xtmp = x + dx[dir];
    ytmp = y + dy[dir];
    if (l.estPositionValide(xtmp,ytmp)) {
        x = xtmp;
        y = ytmp;
    }
    else dir = rand()%4;*/

}

