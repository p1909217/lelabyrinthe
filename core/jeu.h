#ifndef _JEU_H
#define _JEU_H

#include "Personnage.h"
#include "Labyrinthe.h"
#include "Ennemi.h"
#include "Teleporteur.h"
#include "cle.h"
#include "porte.h"

class Jeu{

private :

    Labyrinthe lab;
    Personnage pers;
    Ennemi en;
    Teleporteur tel;
    Cle cl;
    Porte por;

public :

    Jeu();

    Labyrinthe& getLabyrinthe();

    Personnage& getPersonnage();

    const Labyrinthe& getConstLabyrinthe() const;

    const Personnage& getConstPersonnage() const;

    const Ennemi& getConstEnnemi() const;

    const Teleporteur& getConstTeleporteur() const;

    const Cle& getConstCle() const;

    const Porte& getConstPorte() const;

    int getNombreEnnemi() const;

    void actionsAuto();

    void teleporterr();

    void mortt();

    bool actionClavier(const char touche);

    void ouvrir();

};



#endif // _JEU_H
