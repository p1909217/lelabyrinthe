#include "porte.h"
#include <math.h>

Porte::Porte(){

    x=13; y=5;
}

int Porte::porGetX () const { return x; }

int Porte::porGetY () const { return y; }

void Porte::porSetX (unsigned int px)  { x = px; }

void Porte::porSetY (unsigned int py)  { y = py; }

