#ifndef _LABYRINTHE_H
#define _LABYRINTHE_H

class Labyrinthe{

private :

    int dimx, dimy;
    char lab[100][100];
    int numerolab;

public :

    Labyrinthe();

    bool estPositionValide(const int x, const int y) const;

    void ramasseCle(const int x, const int y);

    char getXY (const int x, const int y) const;

    int getDimX() const;

    int getDimY() const;

};



#endif // _LABYRINTHE_H
