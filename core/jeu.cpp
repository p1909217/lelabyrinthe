#include "Jeu.h"


Jeu::Jeu () : lab(),pers(), en(), tel(), cl(), por(){} //enn() {{
    //lab.ramasseArme(pers.getX(),pers.getY());
//}/

Labyrinthe& Jeu::getLabyrinthe() { return lab; }

Personnage& Jeu::getPersonnage() {return pers; }

const Labyrinthe& Jeu::getConstLabyrinthe () const { return lab; }

const Personnage& Jeu::getConstPersonnage () const { return pers; }

const Ennemi& Jeu::getConstEnnemi () const { return en; }

const Teleporteur& Jeu::getConstTeleporteur() const {return tel;}

const Cle& Jeu::getConstCle() const {return cl;}

const Porte& Jeu::getConstPorte() const {return por;}

int Jeu::getNombreEnnemi() const { return 1; }


bool Jeu::actionClavier (const char touche) {
    switch(touche) {
        case 'g' :
            pers.persGauche(lab);
                break;
        case 'd' :
                pers.persDroite(lab);
                break;
        case 'h' :
                pers.persHaut(lab);
                break;
        case 'b' :
                pers.persBas(lab);
                break;/*Jeu::Jeu () : lab(), pers(){} /enn(){ {
    lab.ramasseArme(pers.persGetX(),pers.persGetY());*/
}

}

void Jeu::teleporterr() {
    tel.teleporter(pers, lab);
}

void Jeu::mortt() {
    pers.Mort(en);
}


void Jeu::actionsAuto() {
    en.bougeAuto(lab);
}

void Jeu::ouvrir() {
    cl.ouvrirporte(pers,por,lab);
}

