#include "personnage.h"
#include "porte.h"
#include "labyrinthe.h"
#ifndef CLE_H_INCLUDED
#define CLE_H_INCLUDED

class Cle{

private :

    int x, y;

public :

     Cle();
     int clGetX() const;
     int clGetY() const;
     void clSetX(unsigned int px) ;
     void clSetY(unsigned int py) ;
     void ouvrirporte(const Personnage &p, Porte &por, Labyrinthe &l);

};

#endif // CLE_H_INCLUDED
