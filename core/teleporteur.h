#ifndef _TELEPORTEUR_H
#define _TELEPORTEUR_H
#include "Labyrinthe.h"
#include "personnage.h"

class Teleporteur{

private :

    int x, y;

public :

     Teleporteur();
     void teleporter(Personnage &p, const Labyrinthe &l);
     int telGetX() const;
     int telGetY() const;

};


#endif // _CLE_H

