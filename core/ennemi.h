#ifndef _ENNEMI_H
#define _ENNEMI_H
#include "Labyrinthe.h"

class Ennemi{

private :

    int x, y;
    int dir;

public :

    Ennemi();

    void bougeAuto(const Labyrinthe &l);
    void gauche (const Labyrinthe &l);
    void droite (const Labyrinthe &l);
    void haut (const Labyrinthe &l);
    void bas (const Labyrinthe &l);

    int EnnGetX () const;
    int EnnGetY () const;

};




#endif // _ENNEMI_H

