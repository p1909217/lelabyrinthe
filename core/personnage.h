#ifndef _PERSONNAGE_H
#define _PERSONNAGE_H
#include "Labyrinthe.h"
#include "ennemi.h"

class Personnage {

private :

    int x,y;

public :

    Personnage();

    void persGauche(const Labyrinthe & l);

    void persDroite(const Labyrinthe & l);

    void persHaut(const Labyrinthe & l);

    void persBas(const Labyrinthe & l);

    int persGetX() const;

    int persGetY()const;

    void persSetX(unsigned int px) ;

    void persSetY(unsigned int py) ;

    void Mort(const Ennemi &en) ;

    //void attaquer();

 /*   void ouvrePorte(const Cle &c, const Porte &p);

    void ouvrirPorte();*/


};
#endif // _PERSONNAGE_H


