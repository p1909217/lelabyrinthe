#ifndef PORTE_H_INCLUDED
#define PORTE_H_INCLUDED

class Porte{

private :

    int x, y;

public :

     Porte();
     int porGetX() const;
     int porGetY() const;
     void porSetX(unsigned int px) ;
     void porSetY(unsigned int py) ;

};


#endif // PORTE_H_INCLUDED
