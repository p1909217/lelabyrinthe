#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"

#include "Jeu.h"

void txtAff(WinTXT & win, const Jeu & jeu) {
    const Labyrinthe& lab = jeu.getConstLabyrinthe();
    const Personnage& pers = jeu.getConstPersonnage();
    const Ennemi& en = jeu.getConstEnnemi();
    const Teleporteur& tel = jeu.getConstTeleporteur();
    const Cle& cl = jeu.getConstCle();
    const Porte& por = jeu.getConstPorte();

    win.clear();

    // Affichage des murs et des pastilles
    for(int x=0;x<lab.getDimX();++x)
        for(int y=0;y<lab.getDimY();++y)
            win.print(x,y,lab.getXY(x,y));

    // Affichage du Personnage
    win.print(pers.persGetX(),pers.persGetY(),'$');
    // Affichage de l'Ennemi
    win.print(en.EnnGetX(),en.EnnGetY(),'E');
    // Affichage du Teleporteur
    win.print(tel.telGetX(),tel.telGetY(),'T');
    // Affichage du cle
    win.print(cl.clGetX(),cl.clGetY(),'C');
    // Affichage du porte
    win.print(por.porGetX(),por.porGetY(),'P');

    win.draw();
}

void txtBoucle (Jeu & jeu) {
    // Creation d'une nouvelle fenetre en mode texte
    // => fenetre de dimension et position (WIDTH,HEIGHT,STARTX,STARTY)
    WinTXT win (jeu.getConstLabyrinthe().getDimX(),jeu.getConstLabyrinthe().getDimY());

    bool ok = true;
    int c;

    do {
        txtAff(win,jeu);

        #ifdef _WIN32
        Sleep(100);
        #else
        usleep(100000);
        #endif // WIN32

        jeu.actionsAuto();
        jeu.teleporterr();
        jeu.mortt();
        jeu.ouvrir();

        c = win.getCh();
        switch (c) {
            case 'q':
                jeu.actionClavier('g');
                break;
            case 'd':
                jeu.actionClavier('d');
                break;
            case 's':
                jeu.actionClavier('h');
                break;
            case 'z':
                jeu.actionClavier('b');
                break;
            case 'x':
                ok = false;
                break;
        }

    } while (ok);

}
